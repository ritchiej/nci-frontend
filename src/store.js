
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    config: {
      serviceUrl: 'https://r2.smarthealthit.org',
      patientId: ''
    },
    providers: []
  },
  mutations: {
    setPatientId(state, patientId) {
      state.config.patientId = patientId
    },
    setProviders(state, providers) {
      state.providers = providers
    }
  }
})

export default store;
