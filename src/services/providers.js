import store from '@/store'
// import FHIR from 'fhirclient'
// console.log(FHIR)
import axios from 'axios'

async function getProviders(patientId) {
  if (patientId) {
    store.commit('setPatientId', patientId)
  }

  console.log(store.state.config)

  const smart = FHIR.client(store.state.config)
  const patient = await smart.patient.read()
  const zip = patient.address[0].postalCode
  console.log(zip)

  const response = await axios.get("?zip=" + zip)

  // console.log(response)
  return response.data
}

export default {
  getProviders
}
